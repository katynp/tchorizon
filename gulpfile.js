/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * The gulp script to build the library and examples.
 * @version 1.0
 * @author TCSASSEMBLER
 */
var gulp = require('gulp');
var gReact = require('gulp-react');
var del = require('del');

var webpack = require('gulp-webpack');
var webpackConfig = require('./webpack.config.js');
var connect = require('gulp-connect');

var port = process.env.PORT || 8080;
var reloadPort = process.env.RELOAD_PORT || 35729;

var buildDest = 'build';
var buildExamplesDest = 'build_examples';

// Task to clean
gulp.task('clean', function () {
    del.sync([buildDest, buildExamplesDest]);
});

// Task to build the library
gulp.task('build', ['clean'], function () {
    return gulp.src('src/*.js')
        .pipe(gReact({harmony: true}))
        .pipe(gulp.dest(buildDest));
});

// task to build examples
gulp.task('build-examples', ['build'], function () {
    return gulp.src(webpackConfig.entry["webgl"][0])
        .pipe(webpack(webpackConfig))
        .pipe(gulp.dest(buildExamplesDest));
});

// task to serve examples
gulp.task('serve-examples', function () {
    connect.server({
        port: port,
        livereload: {
            port: reloadPort
        }
    });
});

// task to reload JS
gulp.task('reload-js', function () {
    return gulp.src('./build/*.js')
        .pipe(connect.reload());
});

gulp.task('watch', function () {
    gulp.watch([buildDest + '/*.js', buildExamplesDest + '/*.js'], ['reload-js']);
});

gulp.task('default', ['build-examples', 'serve-examples', 'reload-js']);