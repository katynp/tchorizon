/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is the Quad react component implementation.
 *
 * @providesModule Quad
 * @version 1.0
 * @author albertwang, TCSASSEMBLER
 */

'use strict';

var ContainerMixin = require('./ContainerMixin');
var QuadMixin = require('./QuadMixin');
var LayerMixin = require('./LayerMixin');
var createComponent = require('./createComponent');


// Declare the Quad component
var Quad = createComponent('Quad', LayerMixin, ContainerMixin, QuadMixin);

module.exports = Quad;