/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * AnimationMixin provides implementation for animation playback.
 *
 * @providesModule AnimationMixin
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

var webglInstance = require('./WebGL').webglInstance;

// Declare the mixin
var AnimationMixin = {

	/**
	 * This method is used to start animation playback.
	 * @param target the animation target, including the transform target and the 
	 * transition duration
	 * @param oncomplete the animation complete callback
	 * @param onupdate the aniamtion update callback
	 */
	animate: function (target, onupdate, oncomplete) {
		if (this._animating || !webglInstance.instance) {
			return;
		}

		var self = this;
		this._animating = true;
		webglInstance.instance.animate(target.transform, target.transition, function () {
			self._animating = false;
			if (oncomplete !== undefined) {
	            oncomplete.call(this);
	        }
		}, onupdate);
	}
}

module.exports = AnimationMixin;