/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * DrawingUtils provides utility methods for WebGL based rendering.
 *
 * Adapted from react-canvas (https://github.com/Flipboard/react-canvas).
 *
 * @providesModule DrawingUtils
 * @version 1.0
 * @author react-canvas(https://github.com/Flipboard/react-canvas), albertwang, TCSASSEMBLER
 */

'use strict';

/**
 * This is the vertex shader for the color program to draw 2D graphics.
 * @private
 */
var _2DColorVertexShader = null;

/**
 * This is the fragment shader for the color program to fill RGBA color.
 * @private
 */
var _2DColorFragmentShader = null;

/**
 * The color program that includes information of the WebGL program to draw simple 2D graphics: the WebGL program and various
 * parameter locations.
 * @private
 */
var _2DColorProgram = null;

/**
 * This is the vertex shader for the texture program to draw 2D graphics.
 * @private
 */
var _2DTextureVertexShader = null;

/**
 * This is the fragment shader for the texture program to fill RGBA color.
 * @private
 */
var _2DTextureFragmentShader = null;

/**
 * The texture program that includes information of the WebGL program to draw texture: the WebGL program and various
 * parameter locations.
 * @private
 */
var _2DTextureProgram = null;

/**
 * Represents the program currently being used.
 * @private
 */
var _currentProgram = null;

/**
 * Represents the width of the canvas.
 * @private
 */
var _canvasWidth = null;

/**
 * Represents the height of the canvas.
 * @private
 */
var _canvasHeight = null;

/**
 * This is a vertex buffer that holds the coordinates of the generic quad (0.0, 0.0) -> (1.0, 1.0)
 * @private
 */
var _quadVertexBuffer = null;

/**
 * This is a array buffer object contains the texture coordinates for the generic quad
 * @private
 */ 
var _quadTextureCoordBuffer = null;

/**
 * This is the cache for the loaded images
 * @private
 */ 
var _cachedImages = {};

/**
 * This is the texture cache for the loaded images
 * @private
 */ 
var _cachedImageTextures = {};

/**
 * The load complete handlers for images
 * @private
 */
var _imageLoadCompleteHandlers = {};

/**
 * Urls for the images waiting to be loaded.
 * @private
 */
var _pendingImages = [];

/**
 * The stack of the clipping rectangles
 * @private
 */
var _clippingStack = [];

/**
 * The stack of the current frame buffers
 * @private
 */
var _frameBufferStack = [];

/**
 * Create and compile a shader.
 * @param {WebGLRenderingContext} ctx the WebGL rendering context
 * @param {string} shaderSource the shader source code
 * @param {number} shaderType The type of shader, VERTEX_SHADER or
 *     FRAGMENT_SHADER.
 * @returns {WebGLShader} the shader
 */
function _compileShader(ctx, shaderSource, shaderType) {
    // Create the shader object
    var shader = ctx.createShader(shaderType);

    // Set the shader source code.
    ctx.shaderSource(shader, shaderSource);

    // Compile the shader
    ctx.compileShader(shader);

    // Check if shader is compiled successfully
    var success = ctx.getShaderParameter(shader, ctx.COMPILE_STATUS);
    if (!success) {
        // Something went wrong during compilation
        var error = ctx.getShaderInfoLog(shader);
        ctx.deleteShader(shader);
        throw new Error("Could not compile shader:" + error);
    }

    return shader;
}

/**
 * Creates a program from 2 shaders.
 *
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @param {WebGLShader} vertexShader A vertex shader.
 * @param {WebGLShader} fragmentShader A fragment shader.
 * @return {WebGLProgram} A program.
 */
function _createProgram(ctx, vertexShader, fragmentShader) {
    // Create a program.
    var program = ctx.createProgram();

    // Attach the shaders
    ctx.attachShader(program, vertexShader);
    ctx.attachShader(program, fragmentShader);

    // Link the program.
    ctx.linkProgram(program);

    // Check if the program is linked successfully
    var success = ctx.getProgramParameter(program, ctx.LINK_STATUS);
    if (!success) {
        // something went wrong with the link
        var error = ctx.getShaderInfoLog(shader);
        ctx.deleteProgram(program);
        throw new Error("Program filed to link:" + error);
    }

    return program;
}

/**
 * Represents the regular expression to parse RGBA string ("rgba(122, 20, 30, 0.1)").
 * @type {RegExp} the regular expression
 * @private
 */
var _reRGBAColor = /^rgb(a)?\(\s*(-?[\d]+)(%)?\s*,\s*(-?[\d]+)(%)?\s*,\s*(-?[\d]+)(%)?\s*,?\s*(-?[\d\.]+)?\s*\)$/;

/**
 * Parse RGBA string.
 * @param value the RGBA string
 * @returns RGBA object
 */
function parseRGBAColor(value) {
    var result = [], match, channel, isPercent, hasAlpha, alphaChannel, sameType;

    if ((match = _reRGBAColor.exec(value))) {
        hasAlpha = match[1], alphaChannel = parseFloat(match[8]);

        if ((hasAlpha && isNaN(alphaChannel)) || (!hasAlpha && !isNaN(alphaChannel))) {
            return false;
        }
        sameType = match[3];
        for (var i = 2; i < 8; i += 2) {
            channel = match[i], isPercent = match[i + 1];

            if (isPercent !== sameType) {
                return false;
            }
            // Clamp and normalize values
            if (isPercent) {
                channel = channel > 100 ? 1 : channel / 100;
                channel = channel < 0 ? 0 : channel;
            } else {
                channel = channel > 255 ? 1 : channel / 255;
                channel = channel < 0 ? 0 : channel;
            }
            result.push(channel);
        }
        result.push(hasAlpha ? alphaChannel : 1.0);
    }

    return {
        r: result[0],
        g: result[1],
        b: result[2],
        a: result[3]
    };
}

/**
 * Get the fragment shader for 2D coloring.
 * @private
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @returns {WebGLShader} the shader
 */
function _get2DColorFragmentShader(ctx) {
    if (_2DColorFragmentShader == null) {
        _2DColorFragmentShader = _compileShader(ctx, "precision mediump float;" +
            "uniform vec4 v_color;" +
            "void main() {" +
            "   gl_FragColor = v_color;" +
            "}",
            ctx.FRAGMENT_SHADER);
    }
    return _2DColorFragmentShader;
}


/**
 * Get the vertex shader for rendering 2D graphics.
 * @private
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @returns {WebGLShader} the shader
 */
function _get2DColorVertexShader(ctx) {
    if (_2DColorVertexShader == null) {
        _2DColorVertexShader = _compileShader(ctx, "attribute vec2 a_vertex_position;" +
            "uniform vec2 u_scale;" +
            "uniform vec2 u_offset;" +
            "uniform vec2 u_resolution;" +
            "uniform float u_flipY;" +
            "void main() {" +
            "  vec2 zeroToOne = ((a_vertex_position * u_scale) + u_offset) / u_resolution;" +
            "  vec2 zeroToTwo = zeroToOne * 2.0;" +
            "  vec2 clipSpace = zeroToTwo - 1.0;" +
            "  gl_Position = vec4(clipSpace * vec2(1, u_flipY), 0, 1);" +
            "}",
            ctx.VERTEX_SHADER
        );
    }
    return _2DColorVertexShader;
}

/**
 * Get the fragment shader for rendering textures.
 * @private
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @returns {WebGLShader} the shader
 */
function _get2DTextureFragmentShader(ctx) {
    if (_2DTextureFragmentShader == null) {
        _2DTextureFragmentShader = _compileShader(ctx,  "precision mediump float;" +
            "varying vec2 v_texture_coord;" +
            "uniform sampler2D u_sampler;" +
            "void main() {" +
            "  vec4 texture_color = texture2D(u_sampler, v_texture_coord);" +
            "  gl_FragColor = texture_color;" +
            "}",
            ctx.FRAGMENT_SHADER);
    }
    return _2DTextureFragmentShader;
}


/**
 * Get the vertex shader for rendering textures.
 * @private
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @returns {WebGLShader} the shader
 */
function _get2DTextureVertexShader(ctx) {
    if (_2DTextureVertexShader == null) {
        _2DTextureVertexShader = _compileShader(ctx, "attribute vec2 a_vertex_position;" +
            "attribute vec2 a_texture_coord;" +
            "uniform vec2 u_scale;" +
            "uniform vec2 u_offset;" +
            "uniform vec2 u_resolution;" +
            "uniform float u_flipY;" +
            "varying vec2 v_texture_coord;" +
            "void main() {" +
            "  vec2 zeroToOne = ((a_vertex_position * u_scale) + u_offset) / u_resolution;" +
            "  vec2 zeroToTwo = zeroToOne * 2.0;" +
            "  vec2 clipSpace = zeroToTwo - 1.0;" +
            "  gl_Position = vec4(clipSpace * vec2(1, u_flipY), 0, 1);" +
            "  v_texture_coord = a_texture_coord;" +
            "}",
            ctx.VERTEX_SHADER
        );
    }
    return _2DTextureVertexShader;
}

/**
 * Computes an orthographic projection matrix.
 * @private
 * @param {number) left Left of the projection area.
 * @param {number) right Right of the projection area.
 * @param {number) bottom Bottom of the projection area.
 * @param {number) top Top of the projection area.
 * @param {number) near Position of the near clip plane.
 * @param {number) far Position of the far clip plane.
 * @returns the projection matrix
 */
function _getOrthoMatrix (left, right, bottom, top, near, far) {   
    var r_width  = 1.0 / (right - left);
    var r_height = 1.0 / (top - bottom);
    var r_depth  = 1.0 / (far - near);
    var x =  2.0 * (r_width);
    var y =  2.0 * (r_height);
    var z = -2.0 * (r_depth);
    var tx = -(right + left) * r_width;
    var ty = -(top + bottom) * r_height;
    var tz = -(far + near) * r_depth;
    
    var m0 = x;
    var m5 = y;
    var m10 = z;
    var m12 = tx;
    var m13 = ty;
    var m14 = tz;
    var m15 = 1.0;

    return [x, 0, 0, 0, 
            0, y, 0, 0, 
            0, 0, z, 0, 
            tx, ty, tz, 1.0];
}

/**
 * Turn to use the color program in the WebGL context.
 * @private
 * Create and initialize the color program if it had no been initialized
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @returns the color program
 */
function _use2DColorProgram(ctx) {
    if (_2DColorProgram == null) {
        // Create the program
        _2DColorProgram = {
            program: _createProgram(ctx, _get2DColorVertexShader(ctx), _get2DColorFragmentShader(ctx))
        };

        _2DColorProgram.positionAttribute = ctx.getAttribLocation(_2DColorProgram.program, "a_vertex_position");
        _2DColorProgram.resolutionLocation = ctx.getUniformLocation(_2DColorProgram.program, "u_resolution");
        _2DColorProgram.scaleLocation = ctx.getUniformLocation(_2DColorProgram.program, "u_scale");
        _2DColorProgram.offsetLocation = ctx.getUniformLocation(_2DColorProgram.program, "u_offset");
        _2DColorProgram.colorUniform = ctx.getUniformLocation(_2DColorProgram.program, "v_color");
        _2DColorProgram.flipYLocation = ctx.getUniformLocation(_2DColorProgram.program, "u_flipY");

        ctx.useProgram(_2DColorProgram.program);
        //ctx.uniform2f(_2DColorProgram.resolutionLocation, _canvasWidth, _canvasHeight);
    }

    if (_currentProgram != _2DColorProgram) {
        ctx.useProgram(_2DColorProgram.program);
        ctx.enableVertexAttribArray(_2DColorProgram.positionAttribute);
        _currentProgram = _2DColorProgram;
    }
    if (_frameBufferStack.length > 0) {
        var fbo = _frameBufferStack[_frameBufferStack.length - 1];
        ctx.uniform1f(_2DColorProgram.flipYLocation, 1);
        ctx.uniform2f(_2DColorProgram.resolutionLocation, fbo.width, fbo.height);
    } else {
        ctx.uniform1f(_2DColorProgram.flipYLocation, -1);
        ctx.uniform2f(_2DColorProgram.resolutionLocation, _canvasWidth, _canvasHeight);
    }
    return _2DColorProgram;
}

/**
 * Turn to use the texture program in the WebGL context.
 * @private
 * Create and initialize the texture program if it had no been initialized
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @returns the texture program
 */
function _use2DTextureProgram(ctx) {
    if (_2DTextureProgram == null) {
        // Create the program
        _2DTextureProgram = {
            program: _createProgram(ctx, _get2DTextureVertexShader(ctx), _get2DTextureFragmentShader(ctx))
        };

        _2DTextureProgram.positionAttribute = ctx.getAttribLocation(_2DTextureProgram.program, "a_vertex_position");
        _2DTextureProgram.textureCoordAttribute = ctx.getAttribLocation(_2DTextureProgram.program, "a_texture_coord");
        _2DTextureProgram.resolutionLocation = ctx.getUniformLocation(_2DTextureProgram.program, "u_resolution");
        _2DTextureProgram.scaleLocation = ctx.getUniformLocation(_2DTextureProgram.program, "u_scale");
        _2DTextureProgram.offsetLocation = ctx.getUniformLocation(_2DTextureProgram.program, "u_offset");
        _2DTextureProgram.samplerUniform = ctx.getUniformLocation(_2DTextureProgram.program, "u_sampler");
        _2DTextureProgram.flipYLocation = ctx.getUniformLocation(_2DTextureProgram.program, "u_flipY");

        ctx.useProgram(_2DTextureProgram.program);
        //ctx.uniform2f(_2DTextureProgram.resolutionLocation, _canvasWidth, _canvasHeight);
    }

    if (_currentProgram != _2DTextureProgram) {

        ctx.useProgram(_2DTextureProgram.program);
        ctx.enableVertexAttribArray(_2DTextureProgram.positionAttribute);
        ctx.enableVertexAttribArray(_2DTextureProgram.textureCoordAttribute);
        _currentProgram = _2DTextureProgram;
    }

    if (_frameBufferStack.length > 0) {
        var fbo = _frameBufferStack[_frameBufferStack.length - 1];
        ctx.uniform1f(_2DTextureProgram.flipYLocation, 1);
        ctx.uniform2f(_2DTextureProgram.resolutionLocation, fbo.width, fbo.height);
    } else {
        ctx.uniform1f(_2DTextureProgram.flipYLocation, -1);
        ctx.uniform2f(_2DTextureProgram.resolutionLocation, _canvasWidth, _canvasHeight);
    }
    return _2DTextureProgram;
}

/**
 * Setup the generic quad vertix buffer.
 * @private
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @returns the quad vertix buffer
 */
function _setupQuadVertexBuffer (ctx) {
    if (null == _quadVertexBuffer) {
        var quadCoords = [
            0.0, 0.0,
            1.0, 0.0,
            0.0, 1.0,
            1.0, 1.0
        ];      
        
        // upload data for vertex buffer
        _quadVertexBuffer = ctx.createBuffer();
        ctx.bindBuffer(ctx.ARRAY_BUFFER, _quadVertexBuffer);
        ctx.bufferData(ctx.ARRAY_BUFFER, new Float32Array(quadCoords), ctx.STATIC_DRAW);
        _quadVertexBuffer.itemSize = 2;
        _quadVertexBuffer.numItems = 4;
    }
}

/**
 * Setup the texture coordinates for the generic quad.
 * @private
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @returns texture coordinates for the generic quad
 */
function _setupQuadTextureCoordBuffer (ctx) {
    if (null == _quadTextureCoordBuffer) {
        var textureCoords = [
            0.0, 0.0,
            1.0, 0.0,
            0.0, 1.0,
            1.0, 1.0,               
        ];
        
        // upload data for texture coordinates buffers
        _quadTextureCoordBuffer = ctx.createBuffer();
        ctx.bindBuffer(ctx.ARRAY_BUFFER, _quadTextureCoordBuffer);
        ctx.bufferData(ctx.ARRAY_BUFFER, new Float32Array(textureCoords), ctx.STATIC_DRAW);
        _quadTextureCoordBuffer.itemSize = 2;
        _quadTextureCoordBuffer.numItems = 4;
    }
    return _quadTextureCoordBuffer;
}

/**
 * Tests whether the specified integer is power of two.
 * @private
 * @param {Integer) x The integer to test.
 * @returns true if this is a power of two integer, false otherwise
 */
function _isPowerOfTwo(x) {
    return (x & (x - 1)) == 0;
}

/**
 * Gets WebGL context for the specified canvas.
 * @private
 * @param {Canvas) canvas The canvas to render on.
 * @returns the WebGL context
 */
function getGLContext(canvas) {
    var ctx = null;
    try {
        ctx = canvas.getContext("webgl", {alpha:false});
        if (ctx == null) {
            ctx = canvas.getContext("experimental-webgl", {alpha:false});
        }
    } catch (e) {
        ctx = canvas.getContext("experimental-webgl", {alpha:false});
    }
    if (null == ctx) {
        throw new Error("Failed to retrieve webgl context.");
    }
    return ctx;
}

/**
 * Sets default WebGL state.
 * @param {WebGLRenderingContext) ctx The WebGL context.
 */
function setDefaultGLState (ctx) {
    // sets clear color
    ctx.clearColor(1.0, 1.0, 1.0, 1.0);
    // disable depth test since depth buffer is not used
    ctx.disable(ctx.DEPTH_TEST);
    // Enable blending
    ctx.enable(ctx.BLEND);
    //ctx.blendFunc(ctx.SRC_ALPHA, ctx.ONE_MINUS_SRC_ALPHA);
    ctx.blendFuncSeparate(ctx.SRC_ALPHA, ctx.ONE_MINUS_SRC_ALPHA, ctx.ONE, ctx.ONE_MINUS_SRC_ALPHA);
    ctx.enable(ctx.SCISSOR_TEST);
}

/**
 * Sets WebGL rendering viewport
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @param {number) canvasWidth The canvas width.
 * @param {number) canvasHeight The canvas height.
 */
function setViewport(ctx, canvasWidth, canvasHeight) {
    _canvasWidth = canvasWidth;
    _canvasHeight = canvasHeight;
    _setupQuadVertexBuffer(ctx);
    _setupQuadTextureCoordBuffer(ctx);
    _clippingStack.push({x: 0, y: 0, w: canvasWidth, h: canvasHeight});
}

/**
 * Sets clipping with specified rectangle
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @param {number} x the x position
 * @param {number} y the y position
 * @param {number} width the width of the clip rectangle
 * @param {number} height the height of the clip rectangle 
 * @param {boolean} renderOnTexture whether it's rendering on texture
 */
function _setClipping(ctx, x, y, width, height, renderOnTexture) {
    ctx.scissor(x, renderOnTexture ? y : _canvasHeight - y - height, width, height);
}

/**
 * Create texture with specified image
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @param {Image} image the image
 * @param {number} the image width
 * @param {number} the image height
 * @return the created texture
 */
function createTexture (ctx, image, imageWidth, imageHeight) {
    var texture = ctx.createTexture();
    ctx.bindTexture(ctx.TEXTURE_2D, texture);
    ctx.texImage2D(ctx.TEXTURE_2D, 0, ctx.RGBA, ctx.RGBA, ctx.UNSIGNED_BYTE, image);
    if (!_isPowerOfTwo(imageWidth) || !_isPowerOfTwo(imageHeight)) {
        ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MAG_FILTER, ctx.LINEAR);
        ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MIN_FILTER, ctx.LINEAR);
        ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_S, ctx.CLAMP_TO_EDGE);
        ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_T, ctx.CLAMP_TO_EDGE);
    } else {
        ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MAG_FILTER, ctx.LINEAR);
        ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MIN_FILTER, ctx.LINEAR_MIPMAP_NEAREST);
        ctx.generateMipmap(ctx.TEXTURE_2D);
    }
    ctx.bindTexture(ctx.TEXTURE_2D, null);
    
    return texture;
}

function _loadNextImage () {
    if (_pendingImages.length > 0) {
        var imageUrl = _pendingImages[0];
        _pendingImages.splice(0, 1);

        var img = new Image ();
        img.src = imageUrl;
        img.onload = function () {
            var handlers = _imageLoadCompleteHandlers[imageUrl];
            if (handlers) {
                handlers.forEach(function (handler) {
                    handler();
                });

                delete _imageLoadCompleteHandlers[imageUrl];
            }

            _loadNextImage();
        }
        img.onerror = img.onabort = function() {
            _loadNextImage();
        };

        _cachedImages [imageUrl] = img;
    }
}

/**
 * Gets texture for the image with the specified url
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @param {string} imageUrl The image url
 * @return the image texture
 */
function getImageTexture (ctx, imageUrl) {
    var texture = _cachedImageTextures [imageUrl];
    if (texture) {
        return texture;
    }

    var image = _cachedImages [imageUrl];
    if (image) {
        if (image.complete) {
            texture = createTexture (ctx, image, image.width, image.height);
            _cachedImageTextures [imageUrl] = texture;
            return texture;
        }
    } else if (!_imageLoadCompleteHandlers[imageUrl]) {
        _pendingImages.push(imageUrl);
        
        // we only load one image at the same time to make sure the frame rate
        // would not degrade too much as the result of image loading
        if (_pendingImages.length == 1) {
            _loadNextImage();
        }

        _imageLoadCompleteHandlers[imageUrl] = [];
    }

    return null;
}

/**
 * Adds load complete handler for the image with the specified url
 * @param {string} imageUrl The image url
 * @param {function} handlerFunction the handler function
 */
function addImageLoadCompleteHandler (imageUrl, handlerFunction) {
    if (_imageLoadCompleteHandlers[imageUrl]) {
        _imageLoadCompleteHandlers[imageUrl].push(handlerFunction);
    }
}

/**
 * Generates texture for the text with specified font options
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @param {number} width The width of the texture
 * @param {number} height The height of the texture
 * @param {string} text The text
 * @param {object} options The font options
 * @return the texture for the text
 */
function createTextTexture(ctx, width, height, text, options) {
    // Parse color
    var color = parseRGBAColor(options.fontColor);
    if (!color || color.a <= 0 || 0 == text.length) {
        return null;
    }
    // creates canvas
    var canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    // draw text on the canvas
    var canvasContext = canvas.getContext('2d');
    canvasContext.textBaseline = 'top';
    canvasContext.fillStyle = options.fontColor;
    canvasContext.strokeStyle = options.fontColor;
    var fontProps = options.fontStyle + ' ' + 
        options.fontWeight + ' ' + options.fontSize + '/' + options.lineHeight + ' ' +
        options.fontFamily;
    canvasContext.font = fontProps;
    canvasContext.fillText(text, 0, 0);

    // generates texture with the canvas
    return createTexture(ctx, canvas, width, height);
}

/**
 * Draw a 2D rectangle.
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @param {number} x the x position
 * @param {number} y the y position
 * @param {number} width the width
 * @param {number} height the height
 * @param {string} rgbaColor the RGBA color string
 */
function draw2DRectangle(ctx, x, y, width, height, rgbaColor) {
    var color = rgbaColor;

    // Skip rectangle with opacity(alpha) <= 0, i.e. completely transparent
    if (color && color.a > 0) {

        // Prepare WebGL program
        var program = _use2DColorProgram(ctx);

        ctx.uniform4fv(program.colorUniform, [color.r, color.g, color.b, color.a]);
        ctx.uniform2f(program.scaleLocation, width, height);
        ctx.uniform2f(program.offsetLocation, x, y);

        ctx.bindBuffer(ctx.ARRAY_BUFFER, _quadVertexBuffer);
        ctx.vertexAttribPointer(program.positionAttribute, _quadVertexBuffer.itemSize, 
            ctx.FLOAT, false, 0, 0);
        
        ctx.drawArrays(ctx.TRIANGLE_STRIP, 0, _quadVertexBuffer.numItems);
    }
}

/**
 * Draw a texture.
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @param {number} x the x position
 * @param {number} y the y position
 * @param {number} width the width
 * @param {number} height the height
 * @param {Texture} texture The texture to draw
 */
function drawTexture(ctx, x, y, width, height, texture) {

    // Prepare WebGL program
    var program = _use2DTextureProgram(ctx);

    ctx.uniform2f(program.scaleLocation, width, height);
    ctx.uniform2f(program.offsetLocation, x, y);
    
    ctx.activeTexture(ctx.TEXTURE0);
    ctx.bindTexture(ctx.TEXTURE_2D, texture);
    ctx.uniform1i(program.samplerUniform, 0);

    ctx.bindBuffer(ctx.ARRAY_BUFFER, _quadVertexBuffer);
    ctx.vertexAttribPointer(program.positionAttribute, _quadVertexBuffer.itemSize, 
        ctx.FLOAT, false, 0, 0);

    ctx.bindBuffer(ctx.ARRAY_BUFFER, _quadTextureCoordBuffer);
    ctx.vertexAttribPointer(program.textureCoordAttribute, _quadTextureCoordBuffer.itemSize, 
        ctx.FLOAT, false, 0, 0);
    
    ctx.drawArrays(ctx.TRIANGLE_STRIP, 0, _quadVertexBuffer.numItems);
}

/**
 * Clear the WebGL context buffer.
 * @param {WebGLRenderingContext) ctx The WebGL context.
 */
function clear(ctx) {
    ctx.clear(ctx.COLOR_BUFFER_BIT);
}

/**
 * Helper method to compute next power of two value for given argument
 *
 * @param x the value for which to compute the next power of two
 */
function _getNextPOT (x) {
    x = parseInt(x);

    var res = 2;
    while(res < x) {
        res *= 2;
    }
    return res;
}

/**
 * Helper method to get the frame buffer object from the layer.
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @param {RenderLayer} layer the layer to draw
 */
function _getFrameBufferObject(ctx, layer) {
    if (layer.fbo) {
        return layer.fbo;
    }

    var rttFramebuffer = ctx.createFramebuffer();
    ctx.bindFramebuffer(ctx.FRAMEBUFFER, rttFramebuffer);

    var rttTexture = ctx.createTexture();
    ctx.bindTexture(ctx.TEXTURE_2D, rttTexture);
    ctx.texImage2D(ctx.TEXTURE_2D, 0, ctx.RGBA, layer.width, layer.height, 0,
        ctx.RGBA, ctx.UNSIGNED_BYTE, null);

    ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MAG_FILTER, ctx.LINEAR);
    ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MIN_FILTER, ctx.LINEAR);
    ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_S, ctx.CLAMP_TO_EDGE);
    ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_T, ctx.CLAMP_TO_EDGE);

    ctx.framebufferTexture2D(ctx.FRAMEBUFFER, ctx.COLOR_ATTACHMENT0, ctx.TEXTURE_2D, rttTexture, 0);

    ctx.bindTexture(ctx.TEXTURE_2D, null);
    ctx.bindFramebuffer(ctx.FRAMEBUFFER, null);
    
    var fbo = {};
    fbo.frameBuffer = rttFramebuffer;
    fbo.texture = rttTexture;
    fbo.width = layer.width;
    fbo.height = layer.height;
    fbo.updateNeeded = true;
    layer.fbo = fbo;
    return fbo;
}

/**
 * Draw the render node to current rendering context.
 *
 * @param {WebGLRenderingContext} ctx the WebGL context
 * @param {RenderLayer} layer the layer to draw
 * @param {Number} translatedX the translated X
 * @param {Number} translatedY the translated Y
 */
function _drawRenderNode(ctx, layer, translatedX, translatedY) {
    var right = translatedX + layer.width;
    var bottom = translatedY + layer.height;
    var renderOnTexture = _frameBufferStack.length > 0;

    if (layer.clip || layer.batch) {
        _clippingStack.push({x: translatedX, y: translatedY, w: layer.width, h: layer.height});
        _setClipping(ctx, translatedX, translatedY, layer.width, layer.height, renderOnTexture);
    }

    if (layer.batch) {
        // clear the texture to transparent
        ctx.clearColor(0, 0, 0, 0);
        ctx.clear(ctx.COLOR_BUFFER_BIT);
    }

    var clipRect = _clippingStack[_clippingStack.length - 1];

    if (translatedX < clipRect.x + clipRect.w && translatedY < clipRect.y + clipRect.h &&
        right > clipRect.x && bottom > clipRect.y) {
        // Draw the current layer
        draw2DRectangle(ctx, translatedX, translatedY, layer.width, layer.height,
            layer.backgroundColor);

        // Draw layer custom stuff if needed
        if (layer.drawRenderLayer && typeof layer.drawRenderLayer === "function") {
            layer.drawRenderLayer(ctx, translatedX, translatedY);
        }
    }

    if (layer.children) {
        layer.children.slice().sort(_sortByZIndexAscending).forEach(function (childLayer) {
            drawRenderLayer(ctx, childLayer, translatedX, translatedY);
        });
    }

    if (layer.clip || layer.batch) {
        // pop current clipping and get the last clip rectangle
        _clippingStack.pop();
        clipRect = _clippingStack[_clippingStack.length - 1];
        // check whether it's still rendering on texture
        renderOnTexture = _frameBufferStack.length > 1;
        // revert the clipping 
        _setClipping(ctx, clipRect.x, clipRect.y, clipRect.w, clipRect.h, renderOnTexture);
    }

    if (layer.batch) {
        // revert the clear function
        ctx.clearColor(1, 1, 1, 1);
    }
}

/**
 * Draw a RenderLayer instance to a WebGL rendering context.
 *
 * @param {WebGLRenderingContext} ctx the WebGL context
 * @param {RenderLayer} layer the layer to draw
 * @param {Number} offsetX the offset X
 * @param {Number} offsetY the offset Y
 */
function drawRenderLayer(ctx, layer, offsetX, offsetY) {
    // Translate position
    var translatedX = (offsetX || 0) + layer.x;
    var translatedY = (offsetY || 0) + layer.y;

    if (layer.batch) {
        // prepare the frame buffer object
        var fbo = _getFrameBufferObject(ctx, layer);

        // render this node and the children to the texture if needed
        if (fbo.updateNeeded) {
            _frameBufferStack.push(fbo);
            // bind the frame buffer to render to the texture
            ctx.bindFramebuffer(ctx.FRAMEBUFFER, fbo.frameBuffer);
            // set the viewport
            ctx.viewport(0, 0, fbo.width, fbo.height);

            _drawRenderNode(ctx, layer, 0, 0);

            _frameBufferStack.pop();
            // revert the frame buffer and the viewport
            if (_frameBufferStack.length > 0) {
                var previousFBO = _frameBufferStack[_frameBufferStack.length - 1];
                ctx.bindFramebuffer(ctx.FRAMEBUFFER, previousFBO.frameBuffer);
                ctx.viewport(0, 0, previousFBO.width, previousFBO.height);
            } else {
                ctx.bindFramebuffer(ctx.FRAMEBUFFER, null);
                ctx.viewport(0, 0, _canvasWidth, _canvasHeight);
            }
            // mark the flag as false
            fbo.updateNeeded = false;
        }

        var right = translatedX + layer.width;
        var bottom = translatedY + layer.height;

        var clipRect = _clippingStack[_clippingStack.length - 1];

        // render the prepared texture
        if (translatedX < clipRect.x + clipRect.w && translatedY < clipRect.y + clipRect.h &&
            right > clipRect.x && bottom > clipRect.y) {
            // we have applied the alpha when rendring the texture so now we change the blend
            // function just as if we are rendering a pre-multiplied alpha texture
            ctx.blendFunc(ctx.ONE, ctx.ONE_MINUS_SRC_ALPHA, ctx.ONE, ctx.ONE_MINUS_SRC_ALPHA);
            // draw the texture
            drawTexture(ctx, translatedX, translatedY, layer.width, layer.height, fbo.texture);
            // revert the blend function
            ctx.blendFuncSeparate(ctx.SRC_ALPHA, ctx.ONE_MINUS_SRC_ALPHA, ctx.ONE, ctx.ONE_MINUS_SRC_ALPHA);
        }
    } else {
        _drawRenderNode(ctx, layer, translatedX, translatedY);  
    }
}

/**
 * This function is used to compare two layer by zIndex.
 * @private
 * @param layerA the first layer to compare
 * @param layerB the second layer to compare
 */
function _sortByZIndexAscending(layerA, layerB) {
    return (layerA.zIndex || 0) - (layerB.zIndex || 0);
}

module.exports = {
    getGLContext: getGLContext,
    setDefaultGLState: setDefaultGLState,
    setViewport: setViewport,
    clear: clear,
    drawRenderLayer: drawRenderLayer,
    draw2DRectangle: draw2DRectangle,
    createTexture: createTexture,
    drawTexture: drawTexture,
    getImageTexture: getImageTexture,
    createTextTexture: createTextTexture,
    parseRGBAColor: parseRGBAColor,
    addImageLoadCompleteHandler: addImageLoadCompleteHandler
};
