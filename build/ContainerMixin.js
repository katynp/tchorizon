/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is a mixin that defines container capabilities.
 *
 * NOTE that not all methods in this mixin are currently used, but they're kept because they will be useful
 * in future releases to manipulate children in concrete react components.
 *
 * Adapted from react-canvas (https://github.com/Flipboard/react-canvas).
 *
 * @providesModule ContainerMixin
 * @version 1.0
 * @author react-canvas(https://github.com/Flipboard/react-canvas), albertwang
 */

'use strict';

var React = require('react');
var ReactMultiChild = require('react/lib/ReactMultiChild');
var assign = require('react/lib/Object.assign');
var emptyObject = require('react/lib/emptyObject');

// Declare the ContainerMixin
var ContainerMixin = assign({}, ReactMultiChild.Mixin, {
    /**
     * Moves a child component to the supplied index.
     *
     * @param {ReactComponent} child Component to move.
     * @param {number} toIndex Destination index of the element.
     */
    moveChild: function (child, toIndex) {
        var childNode = child._mountImage;
        var mostRecentlyPlacedChild = this._mostRecentlyPlacedChild;
        if (mostRecentlyPlacedChild == null) {
            // I'm supposed to be first.
            if (childNode.previousSibling) {
                if (this.node.firstChild) {
                    childNode.injectBefore(this.node.firstChild);
                } else {
                    childNode.inject(this.node);
                }
            }
        } else {
            // I'm supposed to be after the previous one.
            if (mostRecentlyPlacedChild.nextSibling !== childNode) {
                if (mostRecentlyPlacedChild.nextSibling) {
                    childNode.injectBefore(mostRecentlyPlacedChild.nextSibling);
                } else {
                    childNode.inject(this.node);
                }
            }
        }
        this._mostRecentlyPlacedChild = childNode;
    },

    /**
     * Creates a child component.
     *
     * @param {ReactComponent} child Component to create.
     * @param {object} childNode the node to insert.
     */
    createChild: function (child, childNode) {
        child._mountImage = childNode;
        var mostRecentlyPlacedChild = this._mostRecentlyPlacedChild;
        if (mostRecentlyPlacedChild == null) {
            // I'm supposed to be first.
            if (this.node.firstChild) {
                childNode.injectBefore(this.node.firstChild);
            } else {
                childNode.inject(this.node);
            }
        } else {
            // I'm supposed to be after the previous one.
            if (mostRecentlyPlacedChild.nextSibling) {
                childNode.injectBefore(mostRecentlyPlacedChild.nextSibling);
            } else {
                childNode.inject(this.node);
            }
        }
        this._mostRecentlyPlacedChild = childNode;
    },

    /**
     * Removes a child component.
     *
     * @param {ReactComponent} child Child to remove.
     */
    removeChild: function (child) {
        child._mountImage.remove();
        child._mountImage = null;
        this.node.invalidateLayout();
    },

    /**
     * Update children at root.
     * @param nextChildren the children to update
     * @param transaction the reconcile transaction
     */
    updateChildrenAtRoot: function (nextChildren, transaction) {
        this.updateChildren(nextChildren, transaction, emptyObject);
    },

    /**
     * Mount and inject children at root.
     * @param children the children to inject
     * @param transaction the reconcile transaction
     */
    mountAndInjectChildrenAtRoot: function (children, transaction) {
        this.mountAndInjectChildren(children, transaction, emptyObject);
    },

    /**
     * It is overridden to bypass batch updating because it is not necessary.
     *
     * @param {?object} nextChildren the children
     * @param {ReactReconcileTransaction} transaction the reconcile transaction
     */
    updateChildren: function (nextChildren, transaction, context) {
        this._mostRecentlyPlacedChild = null;
        this._updateChildren(nextChildren, transaction, context);
    },

    // Shorthands
    /**
     * Mount and inject children.
     * @param children the children
     * @param transaction the reconcile transaction
     * @param context the context
     */
    mountAndInjectChildren: function (children, transaction, context) {
        var mountedImages = this.mountChildren(
            children,
            transaction,
            context
        );

        // Each mount image corresponds to one of the flattened children
        var i = 0;
        for (var key in this._renderedChildren) {
            if (this._renderedChildren.hasOwnProperty(key)) {
                var child = this._renderedChildren[key];
                child._mountImage = mountedImages[i];
                mountedImages[i].inject(this.node);
                i++;
            }
        }
    }

});

module.exports = ContainerMixin;
