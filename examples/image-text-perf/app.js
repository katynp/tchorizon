/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is a performance demonstration of the <WebGL>, <Quad>, <Text> and <Image> components.
 *
 * @version 1.0
 * @author TCSASSEMBLER
 */

'use strict';

var React = require('react');
var ReactUpdates = require('react/lib/ReactUpdates');

var ReactWebGL = require('react-webgl');

var WebGL = ReactWebGL.WebGL;
var Quad = ReactWebGL.Quad;
var Image = ReactWebGL.Image;
var Text = ReactWebGL.Text;

/**
 * Represents the interval (in milliseconds) to append <Quad>, <Text> or <Image> components to the canvas.
 * @type {number}
 */
var UPDATE_INTERVAL = 1000;

/**
 * Represents the maximum number of components to add.
 * @type {number}
 */
var MAX = 1000;

/**
 * Represents the number of <Quad>, <Text> or <Image> components to add in each round.
 * @type {number}
 */
var STEP = 100;

/**
 * Represents the list of font options to be used in the test.
 * @type {array}
 */
var FONT_OPTIONS_LIST = [
    {fontColor: "rgba(0,0,0,1)",fontSize: "12px",fontStyle: "normal",fontWeight: "normal",lineHeight: "normal",fontFamily: "Arial"},
    {fontColor: "rgba(255,0,0,0.5)",fontSize: "14px",fontStyle: "normal",fontWeight: "normal",lineHeight: "normal",fontFamily: "Georgia"},
    {fontColor: "rgba(0,255,0,0.7)",fontSize: "16px",fontStyle: "italic",fontWeight: "normal",lineHeight: "normal",fontFamily: "Times New Roman"},
    {fontColor: "rgba(0,0,255,0.3)",fontSize: "18px",fontStyle: "normal",fontWeight: "bold",lineHeight: "normal",fontFamily: "Verdana"},
    {fontColor: "rgba(255,255,0,1)",fontSize: "20px",fontStyle: "italic",fontWeight: "normal",lineHeight: "25px",fontFamily: "Arial"},
    {fontColor: "rgba(255,0,0,1)",fontSize: "22px",fontStyle: "normal",fontWeight: "normal",lineHeight: "normal",fontFamily: "Arial"},
    {fontColor: "rgba(0,0,0,1)",fontSize: "24px",fontStyle: "italic",fontWeight: "normal",lineHeight: "normal",fontFamily: "Arial"},
    {fontColor: "rgba(0,222,333,1)",fontSize: "26px",fontStyle: "normal",fontWeight: "bold",lineHeight: "30px",fontFamily: "Courier New"},
    {fontColor: "rgba(222,0,0,1)",fontSize: "28px",fontStyle: "normal",fontWeight: "normal",lineHeight: "normal",fontFamily: "Lucida Console"},
    {fontColor: "rgba(44,0,88,1)",fontSize: "30px",fontStyle: "normal",fontWeight: "bold",lineHeight: "normal",fontFamily: "Arial"}];

/**
 * Represents the list of tests to be used in the test.
 * @type {array}
 */
var TEXTS = ["This is text test 1 ....", "This is text test 2 ....", "This is text test 3 ....", 
    "This is text test 4 ....", "This is text test 5 ....", "This is text test 6 ...."];

/**
 * Represents the list of image urls to be used in the test.
 * @type {array}
 */
var IMAGE_URLS = ["img/image-0.png", "img/image-1.png", "img/image-2.png", "img/image-3.png", "img/image-4.png", 
    "img/image-5.png", "img/image-6.png", "img/image-7.png", "img/image-8.png", "img/image-0.jpg", "img/image-1.jpg", 
    "img/image-2.jpg", "img/image-3.jpg"];

/**
 * Generate random integer in range.
 * @param min the minimum value
 * @param max the maximum value
 * @returns the random integer
 */
function randomInteger(min, max) {
    return parseInt((Math.random() * (max - min + 1)), 10) + min;
}


/**
 * Represents the counter of added components.
 * @type {number}
 */
var counter = 0;

// Create the App component
var App = React.createClass({
    /**
     * Render the component.
     * @returns the component
     */
    render: function() {
        return (
            <WebGL ref="webGL" logEnabled={true}>
                {this.props.children}
            </WebGL>
        );
    },

    /**
     * Set up scheduled task to dynamically add <Quad>, <Text> or <Image> elements to the <WebGL> element.
     */
    componentDidMount: function() {
        var webGL = this.refs.webGL;
        var containerElements = [webGL];
        var task = setInterval(function() {
            var x, y, width, height, backgroundColor, zIndex, element, parentElement, elementType, fontOptions, text, imageUrl;
            // As we add children dynamically, we need to use ReactUpdates.ReactReconcileTransaction to
            // reconcile changes
            var transaction = ReactUpdates.ReactReconcileTransaction.getPooled();
            for (var i = 0; i < STEP; i++) {
                elementType = randomInteger(0, 2);
                parentElement = containerElements[randomInteger(0, containerElements.length - 1)];
                zIndex = randomInteger(0, STEP);

                switch (elementType) {
                case 0: // quad
                    {
                        x = randomInteger(0, webGL.props.width / 2);
                        y = randomInteger(0, webGL.props.height / 2);
                        width = randomInteger(0, webGL.props.width / 2);
                        height = randomInteger(0, webGL.props.height / 2);
                        // Generate the color
                        backgroundColor = 'rgba(' + randomInteger(0, 254) + ',' + randomInteger(0, 254)
                            + ',' + randomInteger(0, 254) + ',' + (i % 2 == 0 ? 1 : Math.random()) + ')';
                        
                        element = <Quad x={x} y = {y} width={width} height={height}
                            backgroundColor={backgroundColor} zIndex={zIndex} />;

                        containerElements.push(element);
                    }
                    break;
                case 1: // image
                    {
                        x = randomInteger(0, webGL.props.width / 3 * 2);
                        y = randomInteger(0, webGL.props.height / 3 * 2);
                        width = height = randomInteger(32, webGL.props.width / 3);
                        imageUrl = IMAGE_URLS[randomInteger(0, IMAGE_URLS.length - 1)];

                        element = <Image src={imageUrl} x={x} y = {y} width={width} height={height}
                            backgroundColor="rgba(0,0,0,0)" zIndex={zIndex} />;

                        containerElements.push(element);
                    }
                    break;
                case 2: // text
                    {
                        x = randomInteger(0, webGL.props.width - 400);
                        y = randomInteger(0, webGL.props.height - 80);
                        width = 400;
                        height = 80;
                        text = TEXTS[randomInteger(0, TEXTS.length - 1)];
                        fontOptions = FONT_OPTIONS_LIST[randomInteger(0, FONT_OPTIONS_LIST.length - 1)];

                        element = <Text x={x} y = {y} width={width} height={height}
                            backgroundColor="rgba(0,0,0,0)" zIndex={zIndex} fontColor={fontOptions.fontColor} 
                            fontSize={fontOptions.fontSize} fontStyle={fontOptions.fontStyle} fontWeight={fontOptions.fontWeight} 
                            fontFamily={fontOptions.fontFamily} lineHeight={fontOptions.lineHeight}>
                                {text}
                            </Text>;
                    }   
                    break;
                default:
                    break;           
                }
                
                transaction.perform(
                    webGL.mountAndInjectChildrenAtRoot,
                    parentElement,
                    [element],
                    transaction
                );
            }
            ReactUpdates.ReactReconcileTransaction.release(transaction);

            // Accumulate the counter and terminate the task once the MAX is reached
            counter += STEP;
            if (counter >= MAX) {
                clearInterval(task);
            }
        }, UPDATE_INTERVAL);
    }
});

React.render(<App/>, document.body);