/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This class defines the cell in the application.
 *
 * @providesModule Cell
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

var React = require('react');
var ReactWebGL = require('react-webgl');

function dawnKeyHandler(){
    var source = new EventSource("http://127.0.0.1:8081/server-sent-events");
    source.onmessage = function(e) {
        var d = JSON.parse(e.data);
        if(d.method === "MWKeyEvent" && d.params.KeyState === "UP"){
            if( [13, 37, 38, 39, 40].indexOf(d.params.KeyCode) !== -1){ // enter, left, right, up, down
                keyHandler({which: d.params.KeyCode });
            }else if(d.params.KeyCode === 427){ // channel up/speed up
                //keyHandler({which: 65 });
            }else if(d.params.KeyCode === 428){ // channel down/speed down
                //keyHandler({which: 90});
            }else if (d.params.KeyCode === 403){ // red butto, switch to canvas demo
                window.location.href = "#"
            }
        }
    };
}

var Quad = ReactWebGL.Quad;
var Image = ReactWebGL.Image;
var Text = ReactWebGL.Text;

/**
 * Constructor for Cell
 */
function Cell (options) {
	this._options = options;
	this._node = null;
	this._x = options.x;
	this._y = options.y;
}

/**
 * Creates the react element for this cell
 * @return the reacte element
 */
Cell.prototype.createElement = function () {
	var options = this._options, item = this._options.item;
	return <Quad key={options.key} width={options.width} height={options.height}
    	x={options.x} y={options.y} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}>
        <Quad id="posterBg" width={192} height={282} x={0} y={0} backgroundColor={options.posterBgColor}
            zIndex={1}>
            <Image src={item.image} x={2} y={2} width={188} height={278}
                backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} />
        </Quad>
        <Text x={4} y={284} width={250} height={30} backgroundColor="rgba(0, 0, 0, 0)"
            zIndex={1} fontSize="19px" fontWeight="200" fontColor="rgba(255, 255, 255, 1)">
            {item.title}
        </Text>
        <Image src="i/imdb.png" x={2} y={314} width={32} height={14}
            backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} />
        <Text x={43} y={310} width={60} height={30} backgroundColor="rgba(0, 0, 0, 0)"
            zIndex={1} fontSize="19px" fontWeight="200" fontColor="rgba(255, 255, 255, 1)">
            {item.rating}
        </Text>
        <Text x={73} y={316} width={60} height={30} backgroundColor="rgba(0, 0, 0, 0)"
            zIndex={1} fontSize="10px" fontColor="rgba(128, 128, 128, 1)">
            {"/10"}
        </Text>
    </Quad>
}

/**
 * Sets position for this cell
 * @params x the x position
 * @params x the y position
 */
Cell.prototype.setPosition = function (x, y) {
	this._node.setPosition(x, y);
    this._x = x;
    this._y = y;
}

/**
 * Namesake instance variable getter
 * @return the instance variable
 */
Cell.prototype.getX = function () {
	return this._x;
}

/**
 * Namesake instance variable getter
 * @return the instance variable
 */
Cell.prototype.getY = function () {
	return this._y;
}

/**
 * Attachs render node to this cell
 * @params node the render node to attach
 */
Cell.prototype.attachRenderNode = function (node) {
	this._node = node;
}

/**
 * Select/unselect this cell
 * @params selected whether to select
 */
Cell.prototype.setSelected = function(selected) {
	if (selected) {
		this._node.getChildById('posterBg').setBackgroundColor({r: 1, g: 0, b: 0, a: 1});
	} else {
		this._node.getChildById('posterBg').setBackgroundColor({r: 179/255, g: 179/255, b: 179/255, a: 0.5});
	}
}

module.exports = Cell;
