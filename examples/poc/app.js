/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is a performance demonstration of the <WebGL>, <Quad>, <Text> and <Image> components.
 *
 * @version 1.0
 * @author TCSASSEMBLER
 */

'use strict';

var React = require('react');
var ReactUpdates = require('react/lib/ReactUpdates');

var ReactWebGL = require('react-webgl');
var Section = require('./Section');

var WebGL = ReactWebGL.WebGL;
var Quad = ReactWebGL.Quad;
var Image = ReactWebGL.Image;
var Text = ReactWebGL.Text;
var AnimationMixin = ReactWebGL.AnimationMixin;

var ROW_HEIGHT = 450;

var COLUMN_WIDTH = 205;

var katyGlobalCount = 0;
var keyPresses = [ 40, 40, 40, 40, 40, 39, 39, 39, 39, 37, 37, 38, 38, 37, 37]
var keyDelays = [ 5000, 5000, 5000, 5000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000]

// Create the App component
var App = React.createClass({

    mixins: [AnimationMixin], 

    /**
     * The animation map for the left, right. up, down animations
     */
    animationMap: { 
        moveLeft: { 
            transform: {x: COLUMN_WIDTH, y: 0}, 
            transition: 400
        }, 
        moveRight: { 
            transform: {x: -COLUMN_WIDTH, y: 0}, 
            transition: 400
        },
        moveUp: { 
            transform: {x: 0, y: ROW_HEIGHT}, 
            transition: 500
        }, 
        moveDown: { 
            transform: {x: 0, y: -ROW_HEIGHT}, 
            transition: 500
        }
    },

    /**
     * Render the component.
     * @returns the component
     */
    render: function() {
        var current = new Date ();
        var days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
        var months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
        var time = (current.getHours () < 10 ? "0" : "") + current.getHours () + ":";
        time += (current.getMinutes () < 10 ? "0" : "") + current.getMinutes ();
        var date = days[current.getDay ()] + " " + (current.getDate () < 10 ? "0" : "") + current.getDate () + " "
            + months[current.getMonth ()];

        var json = this.props.data ? JSON.parse(this.props.data) : {"toppicks": []};
        this._section = new Section({x: -304, y: 0, rowHeight: ROW_HEIGHT, columnWidth: COLUMN_WIDTH,
            rowWidth: COLUMN_WIDTH * 9, toppicks: json.toppicks});

        var sectionElement = this._section.createElement();

        return (
            <WebGL ref="webGL" logEnabled={true} >
                <Image src="i/bg-mist.png" x={0} y={0} width={1280} height={720} backgroundColor="rgba(0, 0, 0, 0)"
                    zIndex={0}  batch={true}>
                    <Quad width={1280} height={160} x={0} y={0} backgroundColor="rgba(55, 55, 55, 0)" zIndex={2} >
                        <Text x={85} y={29} width={200} height={30} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}
                            fontSize="22px" fontWeight="300" fontColor={"rgba(255, 255, 255, 1)"}>
                            ON DEMAND
                        </Text>
                        <Text x={1132} y={26} width={200} height={30} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}
                            fontSize="14px" fontWeight="300" fontColor={"rgba(255, 255, 255, 0.699999988079071)"}>
                            {time}
                        </Text>
                        <Text x={1090} y={45} width={200} height={30} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}
                            fontSize="14px" fontWeight="300" fontColor={"rgba(255, 255, 255, 0.699999988079071)"}>
                            {date}
                        </Text>

                        <Quad width={1100} height={40} x={84} y={76} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}>
                            <Text x={0} y={0} width={250} height={38} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}
                                fontSize="30px" fontWeight="bold" fontColor={"rgba(255, 255, 255, 1)"}>
                                TOP PICKS
                            </Text>
                            <Text x={210} y={0} width={250} height={38} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}
                                fontSize="30px" fontWeight="200" fontColor={"rgba(255, 255, 255, 1)"}>
                                REPLAY
                            </Text>
                            <Text x={380} y={0} width={250} height={38} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}
                                fontSize="30px" fontWeight="200" fontColor={"rgba(255, 255, 255, 1)"}>
                                MY PRIME
                            </Text>
                            <Text x={575} y={0} width={250} height={38} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}
                                fontSize="30px" fontWeight="200" fontColor={"rgba(255, 255, 255, 1)"}>
                                VIDEO STORE
                            </Text>
                            <Text x={820} y={0} width={250} height={38} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}
                                fontSize="30px" fontWeight="200" fontColor={"rgba(255, 255, 255, 1)"}>
                                MY CONTENT
                            </Text>
                        </Quad>
                    </Quad>
                </Image>

                <Quad id='container' width={1280} height={500} x={0} y={220} backgroundColor="rgba(0, 0, 0, 0)" clip={true} zIndex={1} >
                    {sectionElement}
                </Quad>
            </WebGL>
        );
    },

    /**
     * Set up scheduled task to dynamically add <Quad>, <Text> or <Image> elements to the <WebGL> element.
     */
    componentDidMount: function() {
        var self = this;
        var request = new XMLHttpRequest();
        request.open( 'GET', 'data/toppicks.json', true);
        request.addEventListener('load', function (event) {
            self.setProps ({"data": this.response}, function () {
                var webGL = self.refs.webGL;
                var sectionNode = webGL.node.getChildById('container').getChildById('section');
                self._section.attachRenderNode(sectionNode);
            });
        }, false);
        request.send(null);

        (function(){
            var source = new EventSource("http://127.0.0.1:8081/server-sent-events");
            source.onmessage = function(e) {
                var d = JSON.parse(e.data);
                //window.alert('remotekey');
                if(d.method === "MWKeyEvent" && d.params.KeyState === "UP"){
                    if( [13, 37, 38, 39, 40].indexOf(d.params.KeyCode) !== -1){ // enter, left, right, up, down
                        console.log("RC key received..");
                        keyHandler({which: d.params.KeyCode });
                    }else if(d.params.KeyCode === 427){ // channel up/speed up
                        //keyHandler({which: 65 });
                    }else if(d.params.KeyCode === 428){ // channel down/speed down
                        //keyHandler({which: 90});
                    }else if (d.params.KeyCode === 403){ // red butto, switch to canvas demo
                        window.location.href = "#"
                    }
                }
            };
        })();



        // (function my_func2() {
        //   // your code
        //   console.log("timer call:" + katyGlobalCount);
        //   katyGlobalCount = katyGlobalCount + 1;
        //   setTimeout( my_func2, 5000 );
        // })();

        var self = this;
        var keyHandler = function (e) {
            self.handleKeyPress(e);
        }
        document.onkeydown = keyHandler;
        
        (function my_func() {
          console.log("timer call:" + katyGlobalCount);
          katyGlobalCount = katyGlobalCount + 1;

          if(katyGlobalCount == 14){
            katyGlobalCount = 0;
          }

          if(katyGlobalCount>2){
            console.log("faking down");
            var x = {which: keyPresses[katyGlobalCount]};
            keyHandler(x);
          }

          setTimeout( my_func, keyDelays[katyGlobalCount] );
        })();

    },

    /**
     * Help method to handle key press events for animation playback
     */
    handleKeyPress: function(e) { 
        var keyCode = e.which;
        var section = this._section;

        if (keyCode == 38 || keyCode == 40) {
            console.log("UP/DOWN received..");
        } else if (keyCode == 37 || keyCode == 39) {
            console.log("LEFT/RIGHT received..");
        }

        if (keyCode === 38) { // Move up
            if (section.couldMoveUp()) {
                var start = section.getOffsetY();
                this.animate(this.animationMap.moveUp, function () {
                    section.moveTo(0, start + this.y);
                });
            }
        } else if (keyCode === 40) { // Move down.
            if (section.couldMoveDown()) {
                var start = section.getOffsetY();
                this.animate(this.animationMap.moveDown, function () {
                    section.moveTo(0, start + this.y);
                });
            }
        } else if (keyCode === 37) { // Move left.
            var activeRow = section.getActiveRow();
            this.animate(this.animationMap.moveLeft, function () {
                activeRow.moveTo(this.x, 0);
            }, function () {
                activeRow.updateSelection();
            });
        } else if (keyCode === 39) { // Move right.
            var activeRow = section.getActiveRow();
            this.animate(this.animationMap.moveRight, function () {
                activeRow.moveTo(this.x, 0);
            }, function () {
                activeRow.updateSelection();
            });
        }
    }

});

React.render(<App />, document.body);
