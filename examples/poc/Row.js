/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This class defines the row which composes with cells in the application.
 *
 * @providesModule Row
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

var React = require('react');
var ReactWebGL = require('react-webgl');

var Quad = ReactWebGL.Quad;

var Cell = require('./Cell');

/**
 * Constructor for Row
 */
function Row (options) {
	this._cells = [];
	this._node = null;
	this._initX = options.x;
	this._initY = options.y;
	this._offsetX = 0;
	this._offsetY = 0;
	this._height = options.height;
	this._width = options.cellWidth * options.items.length;
	this._cellWidth = options.cellWidth;
	this._rowWidth = options.rowWidth;
	this._selected = options.y === 0 ? true : false;
	this._key = options.key;

	var i, items = options.items;
	for (i = 0; i < items.length; i += 1) {
        var cell = new Cell({
        	key: 'cell' + i + '_' + options.y, 
        	width: options.cellWidth,
    		height: options.height, 
    		x: i * options.cellWidth, 
    		y: 0,
    		posterBgColor: options.y == 0 && i == 2 ? "rgba(255, 0, 0, 1)" : "rgba(179, 179, 179, 0.5)",
    		item: items[i]});
        this._cells.push(cell);
    }
}

/**
 * Creates the react element for this row
 * @return the reacte element
 */
Row.prototype.createElement = function () {
	var cellElements = [];
	this._cells.forEach(function(cell) {
		cellElements.push(cell.createElement());
	});

	return <Quad key={this._key} width={this._rowWidth} height={this._height - 100} x={this._initX} y={this._initY} 
    	backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} batch={true}>
            {cellElements}
        </Quad>;
}

/**
 * Move this section to the specified offset
 * @params offsetX the offset x
 * @params offsetY the offset y
 */
Row.prototype.moveTo = function (offsetX, offsetY) {
	this._node.setPosition(this._initX + offsetX, this._initY + offsetY);
	this._offsetX = offsetX;
	this._offsetY = offsetY;
}

/**
 * Select/unselect this row
 * @params selected whether to select
 */
Row.prototype.setSelected = function(selected) {
	var cellWidth = this._cellWidth;

	if (selected != this._selected) {
		this._cells.forEach(function (cell) {
			if (cell.getX() == 2 * cellWidth) {
				cell.setSelected(selected);
			}
		});
		this._selected = selected;
	}
}

/**
 * Updates cell selection in the row
 */
Row.prototype.updateSelection = function () {
	var x, self = this, y = this._offsetY;

	this._cells.forEach(function (cell, index) {
		x = (cell.getX() + self._offsetX + self._width) % self._width;
		cell.setPosition(x, y);

		if (self._selected) {
			if (cell.getX() == 2 * self._cellWidth) {
				cell.setSelected(true);
			} else {
				cell.setSelected(false);
			}
		}
	});

	this._offsetX = 0;
	this._offsetY = 0;
	this._node.setPosition(this._initX, this._initY);
}

/**
 * Attachs render node to this row
 * @params node the render node to attach
 */
Row.prototype.attachRenderNode = function (node) {
	var index = 0;

	this._node = node;
	this._cells.forEach(function(cell) {
		cell.attachRenderNode(node.children[index ++]);
	});
}

module.exports = Row;